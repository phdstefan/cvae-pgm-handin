import os, sys
import numpy as np
import torch
import torch.utils.data
from models.cvae_pointnet import cvae
from provider import ModelNet40
from utils.pc_utils import *

np.random.seed(42)
torch.manual_seed(42)

os.environ["CUDA_DEVICE_ORDER"] = 'PCI_BUS_ID'
os.environ["CUDA_VISIBLE_DEVICES"] = '0'

def create_dataset():
    test_dataset = ModelNet40("train", channel="first", points=1024)
    
    test_loader = torch.utils.data.DataLoader(test_dataset,
            batch_size = 32,
            shuffle = True,
            num_workers = 2,
            pin_memory = True,
            sampler = None,
            worker_init_fn = lambda work_id:np.random.seed(work_id))

    return test_loader

def main():
    test_loader = create_dataset()
    device = torch.device("cuda") # if conf.cuda else "cpu")
    model = cvae().to(device)

    checkpoint = torch.load('models/trained/10k_epochs/model_epoch_10000.chpk')
    model.load_state_dict(checkpoint['model_state_dict'])
    epoch = checkpoint['epoch']
    loss = checkpoint['loss']
    model.eval()

    latent_space = []
    labels_list = []
    input_list = []
    reconstructed_list = []
    for i, (data, labels) in enumerate(test_loader):
        data = data.numpy()
        data = torch.from_numpy(data.astype(np.float32))
        data = data.to(device)

        labels = labels.numpy()
        labels = torch.from_numpy(labels.astype(np.float32))
        labels = labels.to(device)
                
        reconstructed,_,_,z = model(data, labels)

        input_list.append(data.cpu().detach().numpy())
        reconstructed_list.append(reconstructed.cpu().detach().numpy())        
        latent_space.append(z.cpu().detach().numpy())
        labels_list.append(labels.cpu().detach().numpy())
   
    input_numpy = np.concatenate(input_list,axis=0)
    reconstructed_numpy = np.concatenate(reconstructed_list,axis=0)
    latent_space_numpy = np.concatenate(latent_space,axis=0)
    labels_numpy = np.concatenate(labels_list,axis=0)
    print("Saved {} data samples".format(latent_space_numpy.shape[0]))
    np.save("latent_space.npy", latent_space_numpy)
    np.save("labels.npy", labels_numpy)
    np.save("input.npy", input_numpy)
    np.save("reconstructed.npy", reconstructed_numpy)            

if __name__ == "__main__":
    main()
