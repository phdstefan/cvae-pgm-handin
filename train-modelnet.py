import os
import numpy as np
import torch
import torch.optim as optim
import torch.utils.data
from models.cvae_pointnet import *
from provider import ModelNet40

np.random.seed(42)
torch.manual_seed(42)

os.environ["CUDA_DEVICE_ORDER"] = 'PCI_BUS_ID'
os.environ["CUDA_VISIBLE_DEVICES"] = '0'

def create_dataset():
    train_dataset = ModelNet40("train", channel="first", points=1024)
    
    train_loader = torch.utils.data.DataLoader(train_dataset,
            batch_size = 96,
            shuffle = True,
            num_workers = 2,
            pin_memory = True,
            sampler = None,
            worker_init_fn = lambda work_id:np.random.seed(work_id))

    return train_loader

def train(train_loader, model, criterion, optimizer, epoch, device):
    model.train() # Switch to training mode
    totalLoss = 0
    nBatch = len(train_loader)

    for i, (data, labels) in enumerate(train_loader):
        data = data.to(device)
        labels = labels.to(device)

        # Run data through model and calc loss
        reconstructed, mu, logvar, z = model(data, labels)        
        loss = criterion(reconstructed, data, mu, logvar)

        # Optimize using the loss
        totalLoss += loss.item()
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    print("Epoch: " + str(epoch) + " Total loss: " + str(totalLoss/nBatch))

    # Save model every fifth epoch
    if(epoch % 5 == 0):
        torch.save({
            'epoch': epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': criterion
        }, 'models/model_epoch_' + str(epoch) + '.chpk')
        print('Saved: models/model_epoch_' + str(epoch) + '.chpk')


def main():
    train_loader = create_dataset()
    device = torch.device("cuda")
    model = cvae().to(device)

    optimizer = optim.Adam(model.parameters(), lr=10e-5, weight_decay=5e-4)
    criterion = loss()

    for epoch in range(0, 10001):
        train(train_loader, model, criterion, optimizer, epoch, device)

if __name__ == "__main__":
    main()
