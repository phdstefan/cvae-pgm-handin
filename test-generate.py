import os, sys
import numpy as np
import torch
from models.cvae_pointnet import cvae
from provider import ModelNet40

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.random.seed(42)
torch.manual_seed(42)

os.environ["CUDA_DEVICE_ORDER"] = 'PCI_BUS_ID'
os.environ["CUDA_VISIBLE_DEVICES"] = '0'

def plot_and_save(pts,i):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = pts[0,:]
    y = pts[1,:]
    z = pts[2,:]

    order = np.argsort(-z)
        
    ax.scatter(x[order],y[order],z[order],c=y[order], cmap="viridis")
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xlim3d(-1.0,1.0)
    ax.set_ylim3d(-1.0,1.0)
    ax.set_zlim3d(-1.0,1.0)

    ax.view_init(elev=-45,azim=90)
    ax.dist = 12
    ax.set_axis_off()
    plt.savefig("generated/test"+str(i)+".pdf", dpi=300, bbox_inches = 'tight', pad_inches = 0)

def main():
    device = torch.device("cuda")
    model = cvae().to(device)

    checkpoint = torch.load('models/trained/10k_epochs/model_epoch_10000.chpk')
    model.load_state_dict(checkpoint['model_state_dict'])
    epoch = checkpoint['epoch']
    loss = checkpoint['loss']
    model.eval()

    object_id = 5 # Bottles
    object_id = 6 # Bowls
    #object_id = 10 # Cups
    generated_samples = []
    for i in np.arange(100):
        # Sample latent space vector using N(0,1) distribution
        data = np.random.randn(1,200)
        data = torch.from_numpy(data.astype(np.float32))
        data = data.to(device)

        # Convert object ID to one-hot vector encoding
        labels = np.zeros((1,40))
        labels[0][object_id] = 1
        labels = torch.from_numpy(labels.astype(np.float32))
        labels = labels.to(device)

        # Generate sample
        generated = model.decoder(data, labels)
        generated_samples.append(generated.detach().cpu().numpy()[0])
        plot_and_save(generated_samples[-1],i)

    np.save("generated.npy", np.array(generated_samples))

if __name__ == "__main__":
    main()
