import matplotlib.pyplot as plt
import numpy as np

def moving_average(a, n=3) :
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

log_path = "../models/trained/10k_epochs/log.txt"

epochs = []
losses = []
with open(log_path) as fp:
    for cnt, line in enumerate(fp):
        chunks = line.split(" ")
        if(chunks[0] != "Epoch:"):
            continue
        epochs.append(int(chunks[1]))
        losses.append(float(chunks[-1]))

epochs = np.array(epochs)
losses = np.array(losses)
losses = moving_average(losses,n=50)
epochs = moving_average(epochs,n=50)

fig = plt.figure(figsize=(8,3.5))

ax = fig.gca()
ax.set_xticks(np.arange(0, 10000, 1000))
ax.set_yticks(np.arange(0, 2.0, 0.2))
ax.set_ylim([0.0, 1.6])

plt.plot(epochs, losses)
plt.grid()
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.tight_layout()
plt.show()
