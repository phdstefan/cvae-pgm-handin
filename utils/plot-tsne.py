import numpy as np
import matplotlib.pyplot as plt
from pypcd import pypcd
from MulticoreTSNE import MulticoreTSNE as TSNE


latent_space = np.load("../latent_space.npy")
labels = np.load("../labels.npy")
labels = np.where(labels == 1)[1] #Convert from one-hot vector to label IDs

embedded = TSNE(n_jobs=7, n_components=2, n_iter=2000).fit_transform(latent_space)

plt.figure(figsize=(6,5))
unique_ids = np.unique(labels)
colors = ("red","yellow","blue")
markers = ("o", "v", "s")

for k,i in enumerate(unique_ids):
    mask = labels == i
    masked_embedded = embedded[mask]
    plt.scatter(masked_embedded[:,0], masked_embedded[:,1], c=colors[k], marker=markers[k])

plt.show()
