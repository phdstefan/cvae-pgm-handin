import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plot_and_save(pts,i):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = pts[0,:]
    y = pts[1,:]
    z = pts[2,:]

    order = np.argsort(-z)
        
    ax.scatter(x[order],y[order],z[order],c=y[order], cmap="viridis")
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    ax.set_xlim3d(-1.0,1.0)
    ax.set_ylim3d(-1.0,1.0)
    ax.set_zlim3d(-1.0,1.0)

    ax.view_init(elev=-45,azim=90)
    ax.dist = 12
    ax.set_axis_off()
    plt.savefig("../reconstructed/test"+str(i)+".pdf", dpi=300, bbox_inches = 'tight', pad_inches = 0)

def main():
    data = np.load("../input.npy")
    reconstructed = np.load("../reconstructed.npy")

    for i,curr_model in enumerate(data):
        plot_and_save(curr_model,str(i)+'in')
        plot_and_save(reconstructed[i],str(i)+'out')
    
if __name__ == "__main__":
    main()
